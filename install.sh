#!/bin/bash

# make the file if it doesn't exist
if [ ! -f $CONF_DIR/set_wallpaper ]; then
	echo -e "#!/bin/bash\nfeh --bg-scale ''" > $CONF_DIR/set_wallpaper
	chmod u+x $CONF_DIR/set_wallpaper
fi

if [ ! -f $CONF_DIR/.Xresources_l ]; then
	touch $CONF_DIR/.Xresources_l
fi

if [ ! -f $CONF_DIR/.xinitrc_l ]; then
	touch $CONF_DIR/.xinitrc_l
fi

python $PKG_DIR/conf-de/theme.py
